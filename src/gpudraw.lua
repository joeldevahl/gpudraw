Unit:Using("core")
Unit:Using("io")
Unit:Using("application")
Unit:Using("render")
Unit:Using("container")
Unit:Using("math")
Unit:Using("resource")
Unit:Using("dl")
Unit:Using("mesh")

function Unit.Init(self)
	self.executable = true
	self.targetname = "gpudraw"
end

function Unit.Build(self)
	if target.family == "windows" then
		self.settings.link.libs:Add("d3d12")
		self.settings.link.libs:Add("dxguid")
		self.settings.link.libs:Add("dxgi")
	end
	local src = Collect(self.path .. "/*.cpp")
	local obj = Compile(self.settings, src)
	local bin = Link(self.settings, self.targetname, obj)
	self:AddProduct(bin)
end
