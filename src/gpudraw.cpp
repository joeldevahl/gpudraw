#include <core/assert.h>
#include <core/defines.h>
#include <core/time.h>
#include <core/hash.h>
#include <container/array.h>
#include <memory/allocator.h>
#include <memory/memory.h>
#include <application/application.h>
#include <application/window.h>
#include <io/file.h>
#include <io/vfs.h>
#include <io/vfs_mount_fs.h>
#include <math/mat4.h>
#include <resource/resource_cache.h>

#include <dl/dl.h>

#include <render.h>
#include "resource_context.h"

#include <stdio.h>

#define DATA_PATH "local/build/" PLATFORM_STRING "/"
#define MAX_ENTITIES 10000

void mesh_register_creator(resource_context_t* resource_context);
void shader_register_creator(resource_context_t* resource_context);
void material_register_creator(resource_context_t* resource_context);

int atec_main(application_t* application)
{
	resource_context_t resource_context = {};
	resource_context.application = application;

	window_create_params_t window_create_params;
	window_create_params.allocator = &allocator_default;
	window_create_params.width = 1280;
	window_create_params.height = 720;
	window_create_params.name = "gpudraw";
	window_t* window = window_create(&window_create_params);
	resource_context.window = window;

	vfs_create_params_t vfs_params;
	vfs_params.allocator = &allocator_default;
	vfs_params.max_mounts = 4;
	vfs_params.max_requests = 32;
	vfs_params.buffer_size = 16 * 1024 * 1024; // 16 MiB
	vfs_t* vfs = vfs_create(&vfs_params);
	resource_context.vfs = vfs;

	vfs_mount_fs_t* vfs_fs_data = vfs_mount_fs_create(&allocator_default, DATA_PATH);
	vfs_result_t vfs_res = vfs_add_mount(vfs, vfs_mount_fs_read_func, vfs_fs_data);
	ASSERT(vfs_res == VFS_RESULT_OK, "failed to add vfs mount");

	dl_ctx_t dl_ctx;
	dl_create_params_t dl_create_params;
	DL_CREATE_PARAMS_SET_DEFAULT(dl_create_params);
	dl_error_t err = dl_context_create(&dl_ctx, &dl_create_params);
	resource_context.dl_ctx = dl_ctx;

	resource_cache_create_params_t resource_cache_params;
	resource_cache_params.allocator = &allocator_default;
	resource_cache_params.vfs = vfs;
	resource_cache_params.max_resources = 1024;
	resource_cache_params.max_resource_handles = 4096;
	resource_cache_params.max_creators = 16;
	resource_cache_params.max_callbacks = 16;
	resource_cache_t* resource_cache = resource_cache_create(&resource_cache_params);
	resource_context.resource_cache = resource_cache;

	render_create_info_t render_create_info;
	render_create_info.allocator = &allocator_default;
	render_create_info.window = window_get_platform_handle(window);
	render_create_info.max_textures = 1;
	render_create_info.max_shaders = 1;
	render_create_info.max_materials = 1;
	render_create_info.max_meshes = 1;
	render_create_info.max_instances = MAX_ENTITIES;
	render_t* render;
	render_result_t render_res = render_create(&render_create_info, &render);
	ASSERT(render_res == RENDER_RESULT_OK, "failed create render");
	resource_context.render = render;

	const size_t texture_width = 64;
	const size_t texture_height = 64;
	const size_t texture_channels = 4;
	const size_t texture_size = texture_width * texture_height * texture_channels;
	uint8_t texture_data[texture_size];
	memset(texture_data, 1, texture_size);

	render_texture_create_info_t texture_create_info;
	texture_create_info.width = texture_width;
	texture_create_info.height = texture_height;
	texture_create_info.data = (void*)texture_data;
	render_texture_id_t texture_id;
	render_res = render_texture_create(render, &texture_create_info, &texture_id);
	ASSERT(render_res == RENDER_RESULT_OK, "failed create texture");

	render_view_create_info_t view_create_info;
	mat4_t projection_matrix = perspective(45.0f, 1280.0f/720.0f, 0.1f, 1000.0f);
	mat4_t view_matrix = lookat(vec3_t(0.0f, 0.0f, 150.0f), vec3_t(0.0f, 0.0f, 0.0f), vec3_t(0.0f, 1.0f, 0.0f));
	memcpy(view_create_info.initial_data.proj_mat, &projection_matrix, sizeof(mat4_t));
	memcpy(view_create_info.initial_data.view_mat, &view_matrix, sizeof(mat4_t));
	render_view_id_t view_id;
	render_res = render_view_create(render, &view_create_info, &view_id);
	ASSERT(render_res == RENDER_RESULT_OK, "failed create view");

	render_target_t targets[] =
	{
		{ 0, 0, 0, "depth buffer" },
	};

	render_attachment_t color_attachments[] =
	{
		{ "back buffer" },
	};

	render_attachment_t depth_attachment =
	{
		"depth buffer",
	};

	render_command_t commands[1];
	commands[0].type = 0; // TODO
	commands[0].draw.dummy = 0; // TODO

	render_pass_t passes[] =
	{
		{ 0, nullptr, &depth_attachment, ARRAY_LENGTH(commands), commands },
		{ ARRAY_LENGTH(color_attachments), color_attachments, &depth_attachment, ARRAY_LENGTH(commands), commands },
		{ ARRAY_LENGTH(color_attachments), color_attachments, nullptr, ARRAY_LENGTH(commands), commands },
	};

	render_script_create_info_t script_create_info;
	script_create_info.num_transient_targets = ARRAY_LENGTH(targets);
	script_create_info.transient_targets = targets;
	script_create_info.num_passes = ARRAY_LENGTH(passes);
	script_create_info.passes = passes;
	render_script_id_t script_id;
	render_res = render_script_create(render, &script_create_info, &script_id);
	ASSERT(render_res == RENDER_RESULT_OK, "failed create script");

	mesh_register_creator(&resource_context);
	shader_register_creator(&resource_context);
	material_register_creator(&resource_context);

	char* precache_text = nullptr;
	size_t precache_size = 0;
	vfs_sync_read(vfs, &allocator_default, "data/precache.txt", (void**)&precache_text, &precache_size);
	precache_text[precache_size - 1] = '\0';

	array_t<resource_handle_t> handles(&allocator_default, 128);
	char* precache_name = strtok(precache_text, "\r\n");
	while (precache_name != nullptr)
	{
		const char* type_str = strrchr(precache_name, '.') + 1;
		if (strcmp(type_str, "vl") != 0) // TODO: remove this early out and select what to precache better
		{
			uint32_t name_hash = hash_string(precache_name);
			uint32_t type_hash = hash_string(type_str);

			vfs_request_t request;
			vfs_res = vfs_begin_request(vfs, precache_name, &request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed to begin request");

			vfs_res = vfs_request_wait_not_pending(vfs, request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed request");

			uint8_t* data = NULL;
			size_t size = 0;
			vfs_res = vfs_request_data(vfs, request, (void**)&data, &size);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed request");

			ASSERT(data, "No data loaded");

			resource_handle_t handle;
			resource_cache_result_t resource_res = resource_cache_create_resource(resource_cache, name_hash, type_hash, data, size, &handle);
			ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
			handles.append(handle);

			vfs_res = vfs_end_request(vfs, request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed to end request");
		}
		precache_name = strtok(nullptr, "\r\n");
	}
	ALLOCATOR_FREE(&allocator_default, precache_text);

	render_kick_upload(render);

	resource_handle_t mesh_handle;
	resource_cache_result_t resource_res = resource_cache_get_by_name(resource_cache, "data/meshes/plane_xy.mesh", &mesh_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	void* mesh_ptr;
	resource_cache_handle_handle_to_pointer(resource_cache, mesh_handle, &mesh_ptr);
	render_mesh_id_t mesh_id = (intptr_t)mesh_ptr;

	resource_handle_t material_handle;
	resource_res = resource_cache_get_by_name(resource_cache, "data/materials/default.material", &material_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	void* material_ptr;
	resource_cache_handle_handle_to_pointer(resource_cache, material_handle, &material_ptr);
	render_material_id_t material_id = (intptr_t)material_ptr;

	render_instance_id_t instance_id[MAX_ENTITIES];
	render_instance_create_info_t instance_create_info =
	{
		mesh_id,
		material_id,
		{
			{ 0 },
		},
	};
	for (size_t i = 0; i < MAX_ENTITIES; ++i)
	{
		float x = (float)(i % 100) - 50;
		float y = (float)(i / 100) - 50;
		mat4_t mat = translate(x, y, 0.0f) * rotate_x(x * 0.01f) * rotate_y(y * 0.01f);
		memcpy(instance_create_info.initial_data.transform, &mat, sizeof(instance_create_info.initial_data.transform));
		render_res = render_instance_create(render, &instance_create_info, &instance_id[i]);
		ASSERT(render_res == RENDER_RESULT_OK, "failed create render instance");
	}

	uint32_t t = 0;
	render_instance_data_t instance_data[MAX_ENTITIES];
	while (application_is_running(application))
	{
		application_update(application);

		for (size_t i = 0; i < MAX_ENTITIES; ++i)
		{
			float x = (float)(i % 100) - 50;
			float y = (float)(i / 100) - 50;
			mat4_t mat = translate(x, y, 0.0f) * rotate_x((x + t) * 0.01f) * rotate_y((y + t) * 0.01f);
			memcpy(instance_data[i].transform, &mat, sizeof(instance_data[i].transform));
		}

		render_instance_set_data(render, MAX_ENTITIES, instance_id, instance_data);

		render_kick_render(render, view_id, script_id);
		t += 1;
	}

	render_wait_idle(render);

	render_script_destroy(render, script_id);
	render_view_destroy(render, view_id);
	render_texture_destroy(render, texture_id);
	render_destroy(render);

	resource_cache_destroy(resource_cache);

	dl_context_destroy(dl_ctx);

	vfs_mount_fs_destroy(vfs_fs_data);
	vfs_destroy(vfs);

	window_destroy(window);

	return 0;
}
