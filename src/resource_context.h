#pragma once

struct resource_context_t
{
	struct application_s* application;
	struct window_s* window;
	struct vfs_s* vfs;
	struct dl_context* dl_ctx;
	struct resource_cache_s* resource_cache;
	struct render_t* render;
};
