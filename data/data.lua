Unit:Using("vertex_layout")
Unit:Using("mesh")
Unit:Using("shader")
Unit:Using("material")

function Unit.BuildTarget(self)

	local data = {
		"data/programs/*.vp",
		"data/programs/*.fp",
		"data/vertex_layouts/*.vl",
		"data/meshes/*.mesh",
		"data/shaders/*.shader",
		"data/materials/*.material",
	}
	for _,d in ipairs(data) do
		for _,r in pairs(Compile(self.settings, Collect(d))) do
			self:AddProduct(r)
		end
	end

	-- TODO: don't write this all the time
	os.execute("mkdir -p " .. PathJoin(target.outdir, "data"))
	file = io.open(PathJoin(target.outdir, "data/precache.txt"), "wb")
	for v in TableWalk(self.target_products[target]) do
		file:write(string.gsub(v, target.outdir .. "/", "") .. "\n")
	end
	file:close()
end
