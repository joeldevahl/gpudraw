Unit:Using("io")
Unit:Using("dl")
Unit:Using("getopt")

function Unit.Init(self)
	self.executable = true
	self.targetname = "shader"
	self.ignore_application_lib = true
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.shaderc = {}
		settings.shaderc.exe = GetHostBinary("shaderc")

		settings.compile.mappings["shader"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, infile)
			local shaderc = settings.shaderc
			local exe = shaderc.exe
			local cmd = exe .. " -o " .. outfile .. " " .. infile

			AddJob(outfile, "shader " .. infile, cmd)
			AddDependency(outfile, shaderc.exe)
			AddDependency(outfile, infile)

			for line in io.lines(infile) do
				local dep_type, file = string.match(line, "//%s*dep%s+(.+)%s+(.+)")
				if dep_type and file then
					if dep_type == "source" then
						AddDependency(outfile, file)
					else
						-- TODO: transform source to binary version
					end
				end
			end

			return outfile
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end

function Unit.Build(self)
	if target.family == "windows" then
		self.settings.link.libs:Add("d3dcompiler")
		self.settings.link.libs:Add("dxguid")
		self.settings.link.libs:Add("d3d12")
	end
	local bin_obj = Compile(self.settings, Collect(self.path .. "/src/*.cpp"))
	local bin = Link(self.settings, self.targetname .. "c", bin_obj)
	self:AddProduct(bin)
end
