Unit:Using("io")
Unit:Using("dl")
Unit:Using("getopt")

function Unit.Init(self)
	self.executable = true
	self.targetname = "material"
	self.ignore_application_lib = true
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.materialc = {}
		settings.materialc.exe = GetHostBinary("materialc")

		settings.compile.mappings["material"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, infile)
			local materialc = settings.materialc
			local exe = materialc.exe
			local cmd = exe .. " -o " .. outfile .. " " .. infile

			AddJob(outfile, "material " .. infile, cmd)
			AddDependency(outfile, materialc.exe)
			AddDependency(outfile, infile)

			for line in io.lines(infile) do
				local dep_type, file = string.match(line, "//%s*dep%s+(.+)%s+(.+)")
				if dep_type and file then
					if dep_type == "source" then
						AddDependency(outfile, file)
					else
						-- TODO: transform source to binary version
					end
				end
			end

			return outfile
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end

function Unit.Build(self)
	local bin_obj = Compile(self.settings, PathJoin(self.path, "src/materialc.cpp"))
	local bin = Link(self.settings, self.targetname .. "c", bin_obj)
	self:AddProduct(bin)
end
