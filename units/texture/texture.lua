Unit:Using("core")
Unit:Using("memory")
Unit:Using("io")
Unit:Using("dl")
Unit:Using("getopt")

function Unit.Init(self)
	self.executable = true
	--self.static_library = true
	self.targetname = "texture"
	self.ignore_application_lib = true
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.texturec = {}
		settings.texturec.exe = GetHostBinary("texturec")

		settings.compile.mappings["mesh"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, infile)
			local texturec = settings.meshc
			local exe = texturec.exe
			local cmd = exe .. " -o " .. outfile .. " " .. infile

			AddJob(outfile, "texture " .. infile, cmd)
			AddDependency(outfile, texturec.exe)
			AddDependency(outfile, infile)

			return outfile
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end

function Unit.Build(self)
		local bin_obj = Compile(self.settings, PathJoin(self.path, "src/texturec.cpp"))
		local bin = Link(self.settings, self.targetname .. "c", bin_obj)
		self:AddProduct(bin)
end
